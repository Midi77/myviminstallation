#!/bin/bash

if [ "$UID" != "0" ] ; then
	echo
	echo "This script must be run as root"
	echo
	exit
fi

REAL_USER_NAME=`logname`
# hard code the user name on chromebook!

if [ "$REAL_USER_NAME" == ""  ] ; then
	REAL_USER_NAME="$1"
fi

if [ "$REAL_USER_NAME" == ""  ] ; then
	echo
	echo "ERROR: Can not find out your real username, please give your real username as the first argument for the script." 
	echo
	exit
fi

HOME_DIRECTORY=`getent passwd $REAL_USER_NAME | cut -d: -f6`

if [ "$HOME_DIRECTORY" == "" ] ; then
	echo
	echo "ERROR: Can not find out the path to the home directory of: '"$REAL_USER_NAME"', can not continue."
	echo
	exit
fi

if [ ! -e "$HOME_DIRECTORY" ] ; then
	echo
	echo "ERROR: Your home directory: "$HOME_DIRECTORY" does not exist, can not continue."
	echo
	exit
fi


# Get the path to this script
FULL_PATH_TO_SCRIPT=$(readlink -f $0)
PATH_TO_SCRIPT_DIR=`dirname $FULL_PATH_TO_SCRIPT`
PATH_TO_MODIFIED_COLOR_SCHEMES_DIR="$PATH_TO_SCRIPT_DIR/modified_colorschemes"

if [ ! -e "$PATH_TO_MODIFIED_COLOR_SCHEMES_DIR" ] ; then
        echo
        echo "ERROR: Directory for the modified color schemes: "$PATH_TO_MODIFIED_COLOR_SCHEMES_DIR" does not exist, can not continue."
        echo
        exit
fi

if [ ! -e "$PATH_TO_SCRIPT_DIR/" ] ; then
        echo
        echo "ERROR: Your home directory: "$HOME_DIRECTORY" does not exist, can not continue."
        echo
        exit
fi



# Start intallation
echo
echo "This program will install VIM à la Tom:"
echo
echo
echo "If you don't want this then press ctrl + c now."
echo
read -p "Press [Enter] key to start.."
echo



# Remove previous vim installations and install dependencies
echo "Removing apt vim packages..."
echo "--------------------------------------------------------------------------------"
apt-get -y purge vim vim-addon-manager vim-common vim-tiny vim-runtime vim-nox
VIM_PATH=`which vim`
if [ -e "$VIM_PATH" ] ; then rm -rf "$VIM_PATH" ; fi
echo
echo "Removing old vim config files..."
echo "--------------------------------------------------------------------------------"
rm -rf /usr/share/vim*
rm -rf "$REAL_USER_NAME/.vim*"
rm -rf "$REAL_USER_NAME/vim*"
echo
echo "Installing dependencies with apt-get ..."
echo "--------------------------------------------------------------------------------"
apt-get -y install git mercurial python3 python3-dev python python-dev libncurses5-dev build-essential rxvt-unicode-256color xfonts-terminus xclip



# Compile and install vim.
echo
echo "Compiling and installing vim..."
echo "--------------------------------------------------------------------------------"
cd $HOME_DIRECTORY
rm -rf vim
hg clone http://vim.googlecode.com/hg/ vim
cd vim
./configure --with-features=huge --enable-python3interp --enable-pythoninterp --enable-multibyte --disable-gui --prefix=/usr
make -j4
checkinstall
cd $HOME_DIRECTORY
rm -rf vim



# Get the version number of Vim
VIM_OUTPUT=`vim --version | awk '{ print $5 ; exit  }'`
VIM_TEMP=`echo $VIM_OUTPUT | sed 's/\.//'`
VIM_VERSION="vim"$VIM_TEMP
echo
echo "The version number of Vim is "$VIM_OUTPUT
echo "--------------------------------------------------------------------------------"



# Remove forced default indentation settings from Vim filetype plugins
echo
echo "Removing forced default indentation settings from Vim filetype plugins ..."
echo "--------------------------------------------------------------------------------"
for OLD_FILENAME in /usr/share/vim/$VIM_VERSION/ftplugin/*
do
	NEW_FILENAME="$OLD_FILENAME".orig
	mv "$OLD_FILENAME" "$NEW_FILENAME"

	# Remove all lines containing one of the setting keywords used for controlling Vim indentation.
	cat $NEW_FILENAME | grep -Ev 'expandtab' | grep -Ev 'shitwidth' | grep -Ev 'softtabstop' | grep -Ev 'tabstop' > $OLD_FILENAME
done


# Pathogen is used to load other vim plugins.
echo
echo "Installing Pathogen..."
echo "--------------------------------------------------------------------------------"
cd $HOME_DIRECTORY

if [ -e ".vim" ] ; then 
	OLD_VIM_DIR_CREATION_TIME=`ls -dl --time=ctime --time-style=+%F_at_%R .vim | awk '{ print $6 }'`
	OLD_VIM_DIR_NAME=".vim_$OLD_VIM_DIR_CREATION_TIME"
	mv -f .vim $OLD_VIM_DIR_NAME
fi

mkdir -p .vim/autoload .vim/bundle
cd .vim/autoload
wget https://raw.github.com/tpope/vim-pathogen/master/autoload/pathogen.vim
cd $HOME_DIRECTORY
chown -R $REAL_USER_NAME:$REAL_USER_NAME .vim/

# Vim Powerline
echo
echo "Installing Powerline..."
echo "--------------------------------------------------------------------------------"
cd $HOME_DIRECTORY
cd .vim/bundle
git clone https://github.com/Lokaltog/vim-powerline
cd $HOME_DIRECTORY
chown -R $REAL_USER_NAME:$REAL_USER_NAME .vim/


# Ctrlp
echo
echo "Installing Ctrlp..."
echo "--------------------------------------------------------------------------------"
cd $HOME_DIRECTORY
cd .vim/bundle
git clone https://github.com/kien/ctrlp.vim.git
cd $HOME_DIRECTORY
chown -R $REAL_USER_NAME:$REAL_USER_NAME .vim/

# Jedi-vim
echo
echo "installing Jedi for vim"
echo "---------------------------------------------------------------------------------"
cd $HOME_DIRECTORY
cd .vim/bundle
git clone --recursive https://github.com/davidhalter/jedi-vim.git
cd $HOME_DIRECTORY
chown -R $REAL_USER_NAME:$REAL_USER_NAME .vim/

# Python Mode (depreciated)
# echo
# echo "Installing Python Mode..."
# echo "--------------------------------------------------------------------------------"
# cd $HOME_DIRECTORY
# cd .vim/bundle
# git clone git://github.com/klen/python-mode.git
# cd $HOME_DIRECTORY
# chown -R $REAL_USER_NAME:$REAL_USER_NAME .vim/

# Python Folding
echo
echo "Installing Python Folding..."
echo "--------------------------------------------------------------------------------"
mkdir -p ~/.vim/ftplugin
wget -O ~/.vim/ftplugin/python_editing.vim http://www.vim.org/scripts/download_script.php?src_id=5492


# Remove bad looking color schemes that ship with vim
echo
echo "Deleting bad color schemes that ship with vim..."
echo "--------------------------------------------------------------------------------"
rm -v /usr/share/vim/$VIM_VERSION/colors/blue.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/darkblue.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/delek.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/elflord.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/evening.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/koehler.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/morning.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/pablo.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/peachpuff.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/ron.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/shine.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/torte.vim
rm -v /usr/share/vim/$VIM_VERSION/colors/zellner.vim



# Download and install 256 color colorschemes
echo
echo "Installing 256 color schemes..."
echo "--------------------------------------------------------------------------------"
cd /usr/share/vim/$VIM_VERSION/colors/
# Remove old versions of color schemes we are about to download
rm -f jellybeans.vim
rm -f desert256.vim
rm -f distinguished.vim
rm -f solarized.vim
wget http://www.vim.org/scripts/download_script.php?src_id=4055 -O desert256.vim

# Install modified colorschemes aldmeris, distinguished and jellybeans
echo
echo "Installing modified colorschemes Aldmeris, Distinguished and Jellybeans..."
echo "--------------------------------------------------------------------------------"
echo

cp -v $PATH_TO_MODIFIED_COLOR_SCHEMES_DIR/aldmeris.vim /usr/share/vim/$VIM_VERSION/colors/
cp -v $PATH_TO_MODIFIED_COLOR_SCHEMES_DIR/distinguished.vim /usr/share/vim/$VIM_VERSION/colors/
cp -v $PATH_TO_MODIFIED_COLOR_SCHEMES_DIR/jellybeans.vim /usr/share/vim/$VIM_VERSION/colors/



# Write .vimrc
echo
echo "Writing ~/.vimrc"
echo "--------------------------------------------------------------------------------"
cd $HOME_DIRECTORY
cat > .vimrc << 'END_OF_FILE'


" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %


" Better copy & paste
" When you want to paste large blocks of code into vim, press F2 before you
" paste. At the bottom you should see ``-- INSERT (paste) --``.

"" set pastetoggle=<F2>
set clipboard=unnamed


" Mouse and backspace
set mouse=a  " on OSX press ALT and click
set bs=2     " make backspace behave like normal again


" Rebind <Leader> key
" I like to have it here becuase it is easier to reach than the default and
" it is next to ``m`` and ``n`` which I use for navigating between tabs.
let mapleader = ","


" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>


" Quicksave command
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>


" Quick quit command
noremap <Leader>e :quit<CR>  " Quit current window
noremap <Leader>E :qa!<CR>   " Quit all windows


" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


" easier moving between tabs
map <Leader>b <esc>:tabprevious<CR>
map <Leader>n <esc>:tabnext<CR>


" map sort function to a key
vnoremap <Leader>s :sort<CR>


" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation


" Color scheme
" mkdir -p ~/.vim/colors && cd ~/.vim/colors
" wget -O wombat256mod.vim http://www.vim.org/scripts/download_script.php?src_id=13400
set t_Co=256
color jellybeans


" Enable syntax highlighting
" You need to reload this file for the change to apply
filetype off
filetype plugin indent on
syntax on


" Showing line numbers and length
set number  " show line numbers
"" set tw=79   " width of document (used by gd)
set nowrap  " don't automatically wrap on load
set fo-=t   " don't automatically wrap text when typing
" set colorcolumn=80
" highlight ColorColumn ctermbg=233


" easier formatting of paragraphs
vmap Q gq
nmap Q gqap


" Useful settings
set history=700
set undolevels=700


" Real programmers don't use TABs but spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab


" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase


" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile

" Python Folding
set nofoldenable

" No highlighting of matching parentheses
" :let loaded_matchparen = 1

" Automatic double parentheses and curles
inoremap { {}<Left>
inoremap ( ()<Left>
inoremap " ""<Left>
inoremap ' ''<Left>
inoremap [ []<Left>

" Setup Pathogen to manage your plugins
" mkdir -p ~/.vim/autoload ~/.vim/bundle
" curl -so ~/.vim/autoload/pathogen.vim https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim
" Now you can install any plugin into a .vim/bundle/plugin-name/ folder
call pathogen#infect()


" ============================================================================
" Python IDE Setup
" ============================================================================


" Settings for vim-powerline
" cd ~/.vim/bundle
" git clone git://github.com/Lokaltog/vim-powerline.git
set laststatus=2


" Settings for ctrlp
" cd ~/.vim/bundle
" git clone https://github.com/kien/ctrlp.vim.git
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*


" Better navigating through omnicomplete option list
" See http://stackoverflow.com/questions/2170023/how-to-map-keys-for-popup-menu-in-vim
set completeopt=longest,menuone
function! OmniPopup(action)
    if pumvisible()
        if a:action == 'j'
            return "\<C-N>"
        elseif a:action == 'k'
            return "\<C-P>"
        endif
    endif
    return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>


" Python folding
" mkdir -p ~/.vim/ftplugin
" wget -O ~/.vim/ftplugin/python_editing.vim http://www.vim.org/scripts/download_script.php?src_id=5492
set nofoldenable

" Settings for jedi-vim
" cd ~/.vim/bundle
" git clone git://github.com/davidhalter/jedi-vim.git
let g:jedi#usages_command = "<leader>z"
let g:jedi#popup_on_dot = 1
let g:jedi#popup_select_first = 1
map <Leader>b Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>

END_OF_FILE

chown $REAL_USER_NAME:$REAL_USER_NAME .vimrc



echo
echo "Done :)"
echo

